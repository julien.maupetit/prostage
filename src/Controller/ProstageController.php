<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Entreprise;
use App\Entity\Formation;
use App\Entity\Stage;

class ProstageController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
      $listeStages=$this->getDoctrine()->getRepository(Stage::class)->findAll();
      return $this->render("prostage/index.html.twig", [
        "lesStages" => $listeStages,
      ]);
    }

    /**
     * @Route("/formations", name="formations")
     */
    public function listeFormations()
    {
      $listeForm=$this->getDoctrine()->getRepository(Formation::class)->findAll();
      return $this->render("prostage/formations.html.twig", [
        "Formations" => $listeForm
      ]);
    }

    /**
     * @Route("/entreprises", name="entreprises")
     */
    public function listeEntreprises()
    {
        $listeEnt=$this->getDoctrine()->getRepository(Entreprise::class)->findAll();
        return $this->render("prostage/entreprises.html.twig", [
          "Entreprises" => $listeEnt
        ]);
    }

    /**
     * @Route("/stage/{id}", name="stage")
     */
    public function stage(int $id)
    {
        $leStage=$this->getDoctrine()->getRepository(Stage::class)->find($id);
        return $this->render("prostage/stages.html.twig", [
          "unStage" => $leStage,
          "id" => $id
        ]);
    }
}
